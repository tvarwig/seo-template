import type { GetServerSideProps, NextPage } from 'next'
import fs from 'fs'

const Sitemap: NextPage = () => {
  return <></>
}

interface pageType {
  changeFreq: string
  priority: number
  lastmod: string
}

interface pagesType {
  [key: string]: pageType
}

interface sitemapConfigType {
  siteUrl: string
  default: pageType
  pages: pagesType
}

const sitemapConfig: sitemapConfigType = {
  siteUrl: 'https://www.example.com',
  default: {
    changeFreq: 'monthly',
    priority: 0.2,
    lastmod: new Date().toISOString(),
  },
  pages: {
    index: {
      changeFreq: 'monthly',
      priority: 0.9,
      lastmod: '2021-08-03',
    },
    about: {
      changeFreq: 'yearly',
      priority: 0.1,
      lastmod: '2021-08-03',
    },
  },
}

export const getServerSideProps: GetServerSideProps = async ({ res }) => {
  const staticPages = fs
    .readdirSync('src/pages')
    .filter((staticPage) => !['_app.tsx', '_document.tsx', '_error.tsx', 'sitemap.xml.tsx', 'api', 'robots.txt.tsx'].includes(staticPage))
    .map((staticPagePath) => staticPagePath.split('.').slice(0, -1).join('.'))

  const sitemap = `<?xml version="1.0" encoding="UTF-8"?>
  <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    ${staticPages
      .map(
        (page) => `
          <url>
            <loc>${sitemapConfig.siteUrl}/${page === 'index' ? '' : page}</loc>
            <lastmod>${sitemapConfig.pages[page].lastmod || new Date().toISOString()}</lastmod>
            <changefreq>${sitemapConfig.pages[page].changeFreq || sitemapConfig.default.changeFreq}</changefreq>
            <priority>${sitemapConfig.pages[page].priority || sitemapConfig.default.priority}</priority>
          </url>
        `,
      )
      .join('')}
  </urlset>
`

  res.setHeader('Content-Type', 'text/xml')
  res.write(sitemap)
  res.end()

  return {
    props: {},
  }
}

export default Sitemap
