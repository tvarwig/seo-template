import type { GetServerSideProps, NextPage } from 'next'

const Robots: NextPage = () => {
  return <></>
}

export const getServerSideProps: GetServerSideProps = async ({ res }) => {
  const robots = `User-agent: *
Allow: /

User-agent: *
Disallow: /api

Host: ${process.env.SITE_URL}

Sitemap: ${process.env.SITE_URL}/sitemap.xml
  `

  res.setHeader('Content-Type', 'text/plain')
  res.write(robots)
  res.end()

  return {
    props: {},
  }
}

export default Robots
